# JSONServer + JWT Auth

A Fake REST API using json-server with JWT authentication. You can find the [complete tutorial here](https://www.techiediaries.com/fake-api-jwt-json-server/)

## Install
 dentro de la carpeta del proyecto

```bash
$ npm install
$ npm run start-auth
```

