const fs = require('fs')
const bodyParser = require('body-parser')
const jsonServer = require('json-server')
const jwt = require('jsonwebtoken')

const server = jsonServer.create()
const router = jsonServer.router('./database.json')
const userdb = JSON.parse(fs.readFileSync('./users.json', 'UTF-8'))
const geodb = JSON.parse(fs.readFileSync('./geolocalizacion.json', 'UTF-8'))
const operativodb = JSON.parse(fs.readFileSync('./operativo.json', 'UTF-8'))
const listaContribuyentedb = JSON.parse(fs.readFileSync('./listaContribuyente.json', 'UTF-8'))
const constribuyentedb = JSON.parse(fs.readFileSync('./contribuyente.json', 'UTF-8'))
const detalleFacturadb = JSON.parse(fs.readFileSync('./detalleFactura.json', 'UTF-8'))
const fiabilidaddb = JSON.parse(fs.readFileSync('./fiabilidad.json', 'UTF-8'))
const autenticaciondb = JSON.parse(fs.readFileSync('./autenticacion.json', 'UTF-8'))
const catalogoActividades = JSON.parse(fs.readFileSync('./catalogoActividad.json', 'UTF-8'))
const catalogoEdificioPorDepartamento = JSON.parse(fs.readFileSync('./catalogoEdificioPorDepartamento.json', 'UTF-8'))
const catalogoZonaPorDepartamento = JSON.parse(fs.readFileSync('./catalogoZonasPorDepartamento.json', 'UTF-8'))
const catalogoUrbanizacionesPorDepartamento = JSON.parse(fs.readFileSync('./catalogoUrbanizacionesPorDepartamento.json', 'UTF-8'))
const catalogoUbicacionesPorDepartamento = JSON.parse(fs.readFileSync('./catalogoUbicacionesPorDepartamento.json', 'UTF-8'))
const clasificadorDocumentoFiscal = JSON.parse(fs.readFileSync('./clasificadorDocumentoFiscal.json', 'UTF-8'))
const logindb = JSON.parse(fs.readFileSync('./login.json', 'UTF-8'))
const clHorarioAtencion = JSON.parse(fs.readFileSync('./clasificadorHorarioAtencion.json', 'UTF-8'))
const clResultadoVerificacion = JSON.parse(fs.readFileSync('./clasificadorResultadoVerificacion.json', 'UTF-8'))
const clTipoDocumento = JSON.parse(fs.readFileSync('./clasificadorTipoDocumento_fiabilidad.json', 'UTF-8'))
const clTipoLocal = JSON.parse(fs.readFileSync('./clasificadorTipoLocal.json', 'UTF-8'))
const clTipoUbicacion = JSON.parse(fs.readFileSync('./clasificadorTipoUbicacion.json', 'UTF-8'))
const clTipoZonificacion = JSON.parse(fs.readFileSync('./clasificadorTipoZonificacion.json', 'UTF-8'))

server.use(bodyParser.urlencoded({ extended: true }))
server.use(bodyParser.json())
server.use(jsonServer.defaults());

const SECRET_KEY = '123456789'

const expiresIn = '1h'

// Create a token from a payload
function createToken(payload) {
    return jwt.sign(payload, SECRET_KEY, { expiresIn })
}

// Verify the token
function verifyToken(token) {
    return jwt.verify(token, SECRET_KEY, (err, decode) => decode !== undefined ? decode : err)
}

// Check if the user exists in database
function isAuthenticated({ login, password }) {
    return autenticaciondb.users.findIndex(user => user.login === login && user.password === password) !== -1
}

function devolverUsuario(login, password) {
    return autenticaciondb.users.find(user => user.login === login && user.password === password)
}

function verficarToken(token) {
    return userdb.users.findIndex(user => user.token === token) !== -1
}

function obtenerUsuarioPorToken(token) {
    return userdb.users.find(user => user.token === token);
}

//check if the geolocation is in database
function isGeolocalizacion({ nit }) {
    return geodb.geo.findIndex(usergeo => usergeo.nit === nit) !== -1
}

function usergeolocalizado({ nit }) {
    return geodb.geo.find(usergeo => usergeo.nit === nit)
}

function verificarOperativo({ id }) {
    return operativodb.operativo.findIndex(operativo => operativo.id === id) !== -1
}

function obtenerOperativo({ id }) {
    return operativodb.operativo.find(operativo => operativo.id === id);
}

function obtenerListaContribuyente(id) {
    return listaContribuyentedb.listaContribuyente.find(lista => lista.id === id);
}

function verificarContribuyente(id) {
    return constribuyentedb.contribuyente.findIndex(contribuyente => contribuyente.id === id) !== -1;
}

function obtenerContribuyente(id) {
    return constribuyentedb.contribuyente.find(contribuyente => contribuyente.id === id);
}

function esLogeado({ login, password }) {
    return logindb.users.findIndex(user => user.login === login && user.password === password) !== -1
}

function devolverUsuarioLogin(login, password) {
    return logindb.users.find(user => user.login === login && user.password === password)
}
// http://10.1.18.134:8085/rest/detalleVerificacion/obtenerListaContribuyentesUsuarioId/15186

// https://10.1.17.57:39162/rest/catalogoDomicilio/obtenerCatalogoZonasPorDepartamento/2

// https://desasiatrest.impuestos.gob.bo:39160/rest/empadronamientoConsulta/obtenerCatalogosActividades



server.post('/login', (req, res) => {
    const { login, password } = req.body
    console.log(req.body);
    if (esLogeado({ login, password }) === false) {

        res.status(200).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4102,
                        "codigoRelacionado": 0,
                        "descripcion": "Datos de usuario o contraseña no coincide",
                        "descripcionUi": "Datos de usuario o contraseña incorrecta",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC",
                        "siglaSistema": "STR"
                    }
                ],
                "token": null
            }
        )
        return
    }
    const user = devolverUsuarioLogin(login, password);
    res.status(200).json(
        user
    )
})


server.post('/token/get', (req, res) => {
    const { login, password } = req.body
    if (isAuthenticated({ login, password }) === false) {

        res.status(200).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4102,
                        "codigoRelacionado": 0,
                        "descripcion": "Datos de usuario o contraseña no coincide",
                        "descripcionUi": "Datos de usuario o contraseña incorrecta",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC",
                        "siglaSistema": "STR"
                    }
                ],
                "token": null
            }
        )
        return
    }
    const user = devolverUsuario(login, password);
    res.status(200).json(
        {
            "ok": user.ok,
            "mensajes": user.mensajes,
            "token": user.token

        }
    )
})

server.get('/rest/identidad/usuarioPorToken', (req, res) => {
    const authorization = req.headers.authorization;
    token = authorization
    console.log('token: ', token);
    if (verficarToken(token)) {
        const user = obtenerUsuarioPorToken(token);
        res.status(200).json(

                user

        )
        return;
    }
    res.status(200).json(
        {
            "ok": false,
            "mensajes": [
                {
                    "codigo": 4102,
                    "codigoRelacionado": 0,
                    "descripcion": "Datos de usuario o contraseña no coincide",
                    "descripcionUi": "Datos de usuario o contraseña incorrecta",
                    "tipoDestinoId": 1078,
                    "estadoId": "AC",
                    "siglaSistema": "STR"
                }
            ],
            "token": null
        }
    );

});

server.get('/rest/detalleVerificacion/obtenerListaDetalleVerificacionInspeccion/15186', (req, res) => {
    res.status(200).json(
        fiabilidaddb
    )
});

server.get('/rest/empadronamientoConsulta/obtenerCatalogosActividades', (req, res) => {
    res.status(200).json(
        catalogoActividades
    )
});

server.get('/rest/catalogoDomicilio/obtenerCatalogoEdificiosPorDepartamento/2', (req, res) => {
    res.status(200).json(
        catalogoEdificioPorDepartamento
    )
});

server.get('/rest/catalogoDomicilio/obtenerCatalogoZonasPorDepartamento/2', (req, res) => {
    res.status(200).json(
        catalogoZonaPorDepartamento
    )
});

server.get('/rest/catalogoDomicilio/obtenerCatalogoUbicacionesPorDepartamento/2', (req, res) => {
    res.status(200).json(
        catalogoUbicacionesPorDepartamento
    )
});

server.get('/rest/catalogoDomicilio/obtenerCatalogoUrbanizacionesPorDepartamento/2', (req, res) => {
    res.status(200).json(
        catalogoUrbanizacionesPorDepartamento
    )
});

server.get('/parametricas/obtenerDocumentoFiscal', (req, res) => {
    res.status(200).json(
        clasificadorDocumentoFiscal.documentoFiscal
    )
});

server.get('/clasificador/tipoClasificador/tipo_documento_fiabilidad_id', (req, res) => {
    res.status(200).json(
        clTipoDocumento
    )
});

server.get('/clasificador/tipoClasificador/horario_atencion_id', (req, res) => {
    res.status(200).json(
        clHorarioAtencion
    )
});

server.get('/clasificador/tipoClasificador/tipo_local_id', (req, res) => {
    res.status(200).json(
        clTipoLocal
    )
});

server.get('/clasificador/tipoClasificador/tipo_zonificacion_id', (req, res) => {
    res.status(200).json(
        clTipoZonificacion
    )
});

server.get('/clasificador/tipoClasificador/tipo_ubicacion_id', (req, res) => {
    res.status(200).json(
        clTipoUbicacion
    )
});

server.get('/clasificador/tipoClasificador/resultado_verificacion_id', (req, res) => {
    res.status(200).json(
        clResultadoVerificacion
    )
});




server.post('/georeferenciacion', (req, res) => {
    const { nit } = req.body
    if (isGeolocalizacion({ nit }) === false) {
        const status = 200
        const message = 'el nit no esta asociado a ningun contribuyente'
        res.status(status).json({ status, message })
        return
    }
    const georeferenciacion = usergeolocalizado({ nit })
    res.status(200).json(georeferenciacion)
})

server.post('/obtenerContribuyente', (req, res) => {
    console.log("id contribuyente: ", req.body )
    const { id } = req.body;
    if (verificarContribuyente(id) === false) {
        console.log("error", id);
        const status = 200;
        res.status(status).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4100,
                        "codigoRelacionado": 0,
                        "descripcion": "contribuyente no encontrado",
                        "descripcionUi": "contribuyente no encontrado",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC"
                    }
                ]
            }
        );
        return;
    }

    const contribuyente = obtenerContribuyente(id);
    res.status(200).json(
        contribuyente
    );
});

server.post('/obtenerOperativo', (req, res) => {
    const { id } = req.body;
    if (verificarOperativo({ id }) === false) {
        const status = 200;
        res.status(status).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4100,
                        "codigoRelacionado": 0,
                        "descripcion": "operativo no encontrado",
                        "descripcionUi": "operativo no encontrado",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC"
                    }
                ]
            }
        );
        return;
    }
    const operativo = obtenerOperativo({ id })
    const contribuyentes = obtenerListaContribuyente(operativo.contribuyentes).contribuyentes;
    res.status(200).json({
        "ok": true,
        "fechaOperativo": operativo.fechaOperativo,
        "horarioOperativo": operativo.horarioOperativo,
        "distritalAsignada": operativo.distritalAsignada,
        contribuyentes,
        "mensajes": [
            {
                "codigo": 2000,
                "codigoRelacionado": 0,
                "descripcion": "operativo encontrado",
                "descripcionUi": "operativo encontrado",
                "tipoDestinoId": 1078,
                "estadoId": "AC"
            }
        ]
    }
    );


});


server.post('/pin/generar/', (req, res) => {
    res.json(

        {
            "ok": true,
            "mensajes": [
                {
                    "codigo": 2005,
                    "codigoRelacionado": 2000,
                    "descripcion": "Se genero el PIN ",
                    "descripcionUi": "Se ha generado el PIN solicitado",
                    "tipoDestinoId": 1078,
                    "estadoId": "AC"
                }
            ],
            "pinId": 24,
            "duracionPin": 10,
            "estadoId": "AC",
            "estadoPinId": 1250,
            "nit": 1234567890,
            "imeiTelefono": "test1234",
            "numeroTelefono": 77877877,
            "pin": "859409",
            "tipoDuracionPinId": 1248,
            "usuarioRegistroId": 1049,
            "usuarioUltimaModificacionId": 1049,
            "fechaFinPin": 1536086182833,
            "esValido": true
        }

    )
})

server.post('/solicitud/facturacion/recuperarDetalleConsultaFacturas', (req, res) => {
    res.json(


        {
            "ok": true,
            "mensajes": [
                {
                    "codigo": 2046,
                    "codigoRelacionado": 0,
                    "descripcion": "Se recuperÃ³ la informacion de facturas  exitosamente",
                    "descripcionUi": "Se recuperÃ³ la informacion de facturas  exitosamente",
                    "tipoDestinoId": 1078,
                    "estadoId": "AC"
                }
            ],
            "detallesFacturasVentas": [
                {
                    "detalleFacturaId": null,
                    "usuarioRegistroId": null,
                    "usuarioUltimaModificacionId": null,
                    "facturaVentaId": null,
                    "productoId": null,
                    "actividadEconomicaId": null,
                    "codigoProductoInterno": null,
                    "numeroSerie": null,
                    "descripcion": "mesas de madera",
                    "cantidad": 10,
                    "unidadMedida": null,
                    "precioUnitario": 645.141,
                    "montoDescuento": 0,
                    "subTotal": 6450.46,
                    "fechaRegistro": null,
                    "fechaUltimaModificacion": null,
                    "estadoId": null,
                    "visibleRegistro": false
                }
            ]
        }
    )

})


server.get('/rest/opcion/opcionesPorUsuarioTipoAplicacionId/4672/557', (req, res) => {
    res.json(

        [
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1033,
                "tipoOpcionId": 561,
                "nombre": "Transversales",
                "descripcion": "MODULO DE ADMINISTRACION DE TRANSVERSALES",
                "link": null,
                "urlAbsoluta": null,
                "icono": "fa fa-check-circle",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 1,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1035,
                "tipoOpcionId": 562,
                "nombre": "Encriptacion",
                "descripcion": "MODULO DE GESTION DE ENCRIPTACION",
                "link": null,
                "urlAbsoluta": null,
                "icono": "fa fa-circle-o",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 2,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1036,
                "tipoOpcionId": 562,
                "nombre": "Mensajeria",
                "descripcion": "MODULO DE CONFIGURACION DE MENSAJERIA",
                "link": "/correo.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-envelope",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 3,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1037,
                "tipoOpcionId": 562,
                "nombre": "Administrar Opciones",
                "descripcion": "MODULO DE GESTION DE PARMETRICAS DE SISTEMA",
                "link": "/secure/administrarOpciones.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-circle-o",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 4,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1038,
                "tipoOpcionId": 561,
                "nombre": "Parametricas",
                "descripcion": "MODULO DE GESTION DE PARAMETRICAS",
                "link": null,
                "urlAbsoluta": null,
                "icono": "fa fa-check-circle",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 2,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1039,
                "tipoOpcionId": 562,
                "nombre": "Configuración de Clasificadores",
                "descripcion": "MODULO DE GESTION DE CLASIFICADORES",
                "link": null,
                "urlAbsoluta": null,
                "icono": "fa fa-cogs",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 1,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1040,
                "tipoOpcionId": 562,
                "nombre": "Administración de Cites y Códigos",
                "descripcion": "MODULO DE ADMINISTRACION DE CITES Y CODIGOS",
                "link": "/views/Correspondencia/administracionPlantilla.faces",
                "urlAbsoluta": null,
                "icono": "fa fa-check-circle",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 2,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1050,
                "tipoOpcionId": 562,
                "nombre": "Calendario",
                "descripcion": "UTILITARIO DE MANEJO DE CALENDARIO",
                "link": "/calendario.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-calendar",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 5,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1065,
                "tipoOpcionId": 562,
                "nombre": "Búsqueda de Usuarios",
                "descripcion": "MODULO DE BUSQUEDA DE USUARIOS",
                "link": "/busqueda.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-search",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1003,
                "aplicacionId": 1000,
                "ordenDespliegue": 2,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1072,
                "tipoOpcionId": 562,
                "nombre": "Administración de Perfiles",
                "descripcion": "MODULO PARA LA ADMINISTRACION DE PERFILES",
                "link": "/secure/administrarPerfiles.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-circle-o",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 5,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1075,
                "tipoOpcionId": 562,
                "nombre": "Administración de Roles",
                "descripcion": "MODULO DE ADMINISTRACION DE ROLES",
                "link": "/secure/administrarRoles.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-circle-o",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1001,
                "aplicacionId": 1000,
                "ordenDespliegue": 1,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1076,
                "tipoOpcionId": 561,
                "nombre": "USUARIOS",
                "descripcion": "MODULO PARA LA ADMINISTRACION DE USUARIOS",
                "link": null,
                "urlAbsoluta": null,
                "icono": null,
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1003,
                "aplicacionId": 1000,
                "ordenDespliegue": 1,
                "estadoId": "AC",
                "opcionPadreId": null
            },
            {
                "ok": false,
                "mensajes": [],
                "opcionId": 1077,
                "tipoOpcionId": 562,
                "nombre": "Asignación de Perfil",
                "descripcion": "MODULO PARA LA ASIGNACION DE PERFILES A USUARIOS",
                "link": "/secure/asignarPerfilUsuario.xhtml",
                "urlAbsoluta": null,
                "icono": "fa fa-address-card",
                "tipoAplicacionId": 557,
                "subOpcion": [],
                "subsistemaId": 1003,
                "aplicacionId": 1000,
                "ordenDespliegue": 1,
                "estadoId": "AC",
                "opcionPadreId": null
            }
        ]

    )
})


server.post('/verificar_correo', (req, res) => {
    const { duracionPin, identificador, numeroTelefono, tipoDuracionPinId, usuarioId, longitud, tipoOperacionId } = req.body
    console.log(req.body);
    if (identificador == 'millaresmp@gmail.com' || identificador == 'javazelada@gmail.com') {
        console.log('igual', identificador);
        res.status(200).json(
            {
                "ok": true,
                "mensajes": [
                    {
                        "codigo": 2005,
                        "codigoRelacionado": 2000,
                        "descripcion": "Se genero el PIN ",
                        "descripcionUi": "Se ha generado el PIN solicitado",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC",
                        "siglaSistema": "STR"
                    }
                ],
                "pinId": 86,
                "duracionPin": 10,
                "estadoId": "AC",
                "estadoPinId": 1250,
                "nit": null,
                "identificador": "javier.zelada@impuestos.gob.bo",
                "numeroTelefono": 67327357,
                "pin": "183435",
                "tipoDuracionPinId": 1248,
                "tipoOperacionId": 1421,
                "usuarioRegistroId": 1000,
                "usuarioUltimaModificacionId": 1000,
                "fechaFinPin": 1543865864296,
                "esValido": true
            }
        )
    } else {
        res.status(200).json({
            "ok": false,
            "mensajes": [
                {
                    "codigo": 2005,
                    "codigoRelacionado": 2000,
                    "descripcion": "no se genero el PIN ",
                    "descripcionUi": "no se ha generado el PIN solicitado",
                    "tipoDestinoId": 1078,
                    "estadoId": "AC",
                    "siglaSistema": "STR"
                }
            ],
            "pinId": 86,
            "duracionPin": 10,
            "estadoId": "AC",
            "estadoPinId": 1250,
            "nit": null,
            "identificador": "<",
            "numeroTelefono": 0,
            "pin": "",
            "tipoDuracionPinId": 1248,
            "tipoOperacionId": 1421,
            "usuarioRegistroId": 1000,
            "usuarioUltimaModificacionId": 1000,
            "fechaFinPin": 1543865864296,
            "esValido": true
        })
    }

})

server.post('/verificar_correo/pin', (req, res) => {
    const { direccion, pin } = req.body;
    if (pin == '123456' && (direccion == 'javazelada@gmail.com' || direccion == 'millaresmp@gmail.com')) {
        console.log('succesfull', pin);
        res.status(200).json(true)

    } else {
        console.log('error', pin);
        res.status(200).json(false)
    }
})


server.post('/verificar_celular', (req, res) => {
    const { numeroTelefono } = req.body;
    console.log("verificar celular: ", numeroTelefono);
    if (numeroTelefono == "72548784" || numeroTelefono == "67327357") {
        res.status(200).json(
            {
                "ok": true,
                "mensajes": [
                    {
                        "codigo": 2005,
                        "codigoRelacionado": 2000,
                        "descripcion": "Se genero el PIN ",
                        "descripcionUi": "Se ha generado el PIN solicitado",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC",
                        "siglaSistema": "STR"
                    }
                ],
                "pinId": 99,
                "duracionPin": 1,
                "estadoId": "AC",
                "estadoPinId": 1250,
                "nit": null,
                "identificador": "",
                "numeroTelefono": 67327357,
                "pin": "503184",
                "tipoDuracionPinId": 1248,
                "tipoOperacionId": 1421,
                "usuarioRegistroId": 1000,
                "usuarioUltimaModificacionId": 1000,
                "fechaFinPin": 1543959233648,
                "esValido": true
            }
        )

    } else {
        res.status(200).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 2005,
                        "codigoRelacionado": 2000,
                        "descripcion": "No se genero el PIN ",
                        "descripcionUi": "No se ha generado el PIN solicitado",
                        "tipoDestinoId": 1078,
                        "estadoId": "AC",
                        "siglaSistema": "STR"
                    }
                ],
                "pinId": 99,
                "duracionPin": 1,
                "estadoId": "AC",
                "estadoPinId": 1250,
                "nit": null,
                "identificador": "",
                "numeroTelefono": 67327357,
                "pin": "503184",
                "tipoDuracionPinId": 1248,
                "tipoOperacionId": 1421,
                "usuarioRegistroId": 1000,
                "usuarioUltimaModificacionId": 1000,
                "fechaFinPin": 1543959233648,
                "esValido": true
            }
        )
    }

})

server.post('/verificar_celular/pin', (req, res) => {
    const { numeroTelefono, pin } = req.body;
    if (pin == '123456' && (numeroTelefono == '72548784' || numeroTelefono == '67327357')) {
        console.log('succesfull', pin);
        res.status(200).json(true)

    } else {
        console.log('error', pin);
        res.status(200).json(false)
    }

})
server.post('/verificar_persona', (req, res) => {

    const { tipoDocumento, numeroDocumento, complemento, nombre, primerApellido, segundoApellido, fechaNacimiento } = req.body
    if ((tipoDocumento == "Carnet de Identidad") &&
        (numeroDocumento == "2313467" || numeroDocumento == "6800356") &&
        (nombre == "miriam" || nombre == "javier andres") &&
        (primerApellido == "pacheco" || primerApellido == "zelada") &&
        (segundoApellido == "millares" || segundoApellido == "yujra") &&
        (fechaNacimiento == "17/05/1959" || fechaNacimiento == "21/04/1988")) {
        console.log('successfull', req.body);
        res.status(200).json({
            "ok": true,
            "mensajes": [
                {
                    "codigo": 2000,
                    "codigoRelacionado": 0,
                    "descripcion": "Datos correctos",
                    "descripcionUi": "Datos correctos",
                    "tipoDestinoId": 0000,
                    "estadoId": "AC"
                }
            ]
        })
        return
    } else {
        console.log('error', req.body);
        const status = 200
        res.status(status).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4100,
                        "codigoRelacionado": 0,
                        "descripcion": "Datos incorrectos",
                        "descripcionUi": "Datos incorrectos",
                        "tipoDestinoId": 0000,
                        "estadoId": "AC"
                    }
                ]
            }
        )


    }

})


server.post('/cambio_contrasena', (req, res) => {
    console.log("contrasena", req.body);
    const { usuarioId, oldPassword, password } = req.body
    if (usuarioId == 2188
        && oldPassword == "abc123"
        && password.length > 6) {
        res.status(200).json({
            "ok": true,
            "mensajes": [
                {
                    "codigo": 2000,
                    "codigoRelacionado": 0,
                    "descripcion": "Cambio de contraseña exitoso",
                    "descripcionUi": "Cambio de contraseña exitoso",
                    "tipoDestinoId": 0000,
                    "estadoId": "AC"
                }
            ]
        })
        return
    } else {

        const status = 200
        res.status(status).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4100,
                        "codigoRelacionado": 0,
                        "descripcion": "introdusca una contraseña mayor a 6 digitos",
                        "descripcionUi": "introdusca una contraseña mayor a 6 digitos",
                        "tipoDestinoId": 0000,
                        "estadoId": "AC"
                    }
                ]
            }
        )
    }
})

server.post('/verificacion_nit', (req, res) => {
    const { nit } = req.body
    if (nit == "1020703023") {
        res.status(200).json({
            "ok": true,
            "mensajes": [
                {
                    "codigo": 2000,
                    "codigoRelacionado": 0,
                    "descripcion": "Nit validado correctamente",
                    "descripcionUi": "Nit validado correctamente",
                    "tipoDestinoId": 0000,
                    "estadoId": "AC"
                }
            ]
        })
        return
    } else {

        const status = 200
        res.status(status).json(
            {
                "ok": false,
                "mensajes": [
                    {
                        "codigo": 4100,
                        "codigoRelacionado": 0,
                        "descripcion": "Nit invalido",
                        "descripcionUi": "Nit invalido",
                        "tipoDestinoId": 0000,
                        "estadoId": "AC"
                    }
                ]
            }
        )
    }
})

server.post('/registrar_persona', (req, res) => {
    console.log(req.body);
    const { correo, celular, numeroDocumento, complemento, nombre, primerApellido, segundoApellido, fechaNacimiento, nitDependiente, password } = req.body
    if (correo.length > 0 &&
        celular.length > 0 &&
        numeroDocumento.length > 0 &&
        nombre.length > 0 &&
        primerApellido.length > 0 &&
        segundoApellido.length > 0 &&
        fechaNacimiento.length > 0 &&
        password.length > 0
    ) {
        res.status(200).json({
            "ok": true,
            "usuarioId": 2188,
            "login": "javazelada@gmail.com",
            "mensajes": [
                {
                    "codigo": 2000,
                    "codigoRelacionado": 0,
                    "descripcion": "Datos Registrados",
                    "descripcionUi": "Datos Registrados",
                    "tipoDestinoId": 0000,
                    "estadoId": "AC"
                }
            ]
        })
        return
    } else {

        const status = 401
        res.status(status).json(
            {
                "ok": false,
                "usuarioId": 0,
                "login": "",
                "mensajes": [
                    {
                        "codigo": 4100,
                        "codigoRelacionado": 0,
                        "descripcion": "Datos incompletos",
                        "descripcionUi": "Datos incompletos",
                        "tipoDestinoId": 0000,
                        "estadoId": "AC"
                    }
                ]
            }
        )
    }

})

server.use(/^(?!\/auth).*$/, (req, res, next) => {
    if (req.headers.authorization === undefined || req.headers.authorization.split(' ')[0] !== 'Bearer') {
        const status = 401
        const message = 'Error in authorization format'
        res.status(status).json({ status, message })
        return
    }
    try {
        verifyToken(req.headers.authorization.split(' ')[1])
        next()
    } catch (err) {
        const status = 401
        const message = 'Error access_token is revoked'
        res.status(status).json({ status, message })
    }
})

server.use(router)

server.listen(8085, () => {
    console.log('Run Auth API Server, port: 8085')
})

/*
http://10.1.17.27:39107/parametricas/obtenerModalidadFacturacion

[
    {
        "clasificadorId": 791,
        "descripcion": "PREVALORADA",
        "abreviatura": "MODPRE",
        "tipoClasificador": "modalidad_facturacion_id"
    },
    {
        "clasificadorId": 600,
        "descripcion": "COMPUTARIZADA",
        "abreviatura": "MODCOMP",
        "tipoClasificador": "modalidad_facturacion_id"
    },
    {
        "clasificadorId": 601,
        "descripcion": "ELECTRONICA",
        "abreviatura": "MODCOMP",
        "tipoClasificador": "modalidad_facturacion_id"
    },
    {
        "clasificadorId": 602,
        "descripcion": "MANUAL",
        "abreviatura": "MODMAN",
        "tipoClasificador": "modalidad_facturacion_id"
    },
    {
        "clasificadorId": 1244,
        "descripcion": "WEB",
        "abreviatura": "WEB",
        "tipoClasificador": "modalidad_facturacion_id"
    }
]


http://10.1.17.27:39107/parametricas/obtenerDocumentoFiscal


[
    {
        "clasificadorId": 345,
        "descripcion": "FACTURA",
        "abreviatura": "TDFAC",
        "tipoClasificador": "tipo_documento_fiscal_id"
    },
    {
        "clasificadorId": 346,
        "descripcion": "NOTA CREDITO/DEBITO",
        "abreviatura": "TDNCD",
        "tipoClasificador": "tipo_documento_fiscal_id"
    },
    {
        "clasificadorId": 671,
        "descripcion": "NOTA FISCAL",
        "abreviatura": "TDFNF",
        "tipoClasificador": "tipo_documento_fiscal_id"
    },
    {
        "clasificadorId": 911,
        "descripcion": "BOLETA DE CONTINGENCIA",
        "abreviatura": "TDBCON",
        "tipoClasificador": "tipo_documento_fiscal_id"
    },
    {
        "clasificadorId": 925,
        "descripcion": "PRE VALORADA",
        "abreviatura": "TDFPREV",
        "tipoClasificador": "tipo_documento_fiscal_id"
    },
    {
        "clasificadorId": 995,
        "descripcion": "DOCUMENTO EQUIVALENTE",
        "abreviatura": "DOCEQ",
        "tipoClasificador": "tipo_documento_fiscal_id"
    }
]
*/
